package com.example.gopaljee.hackernews2.utils;

/**
 * Created by gopaljee on 03/07/17.
 */

public class UserMessageUtils {

    public static String userMessage(String userName)
    {
        String message ="";
        if(userName !=null && !userName.isEmpty())
        {
            message = "by "+userName.trim();
        }
        return  message;
    }
}
