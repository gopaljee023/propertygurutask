package com.example.gopaljee.hackernews2.presentations.presentors;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.base.BasePresenter;
import com.example.gopaljee.hackernews2.presentations.ui.HackerNewsBaseView;

import java.util.List;

/**
 * Created by gopaljee on 10/07/17.
 */

public interface ICommentPresentor extends BasePresenter {

    interface HackerCommentView extends HackerNewsBaseView
    {
        void addData(CommentDTO data,boolean isChild );
    }
    void fetchComment(List<Integer> id,int start);
    void setNewsInteractor(INewsInteractor newsInteractor);
}
