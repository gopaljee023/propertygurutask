package com.example.gopaljee.hackernews2.presentations.presentors.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.domain.interactor.impl.NewsInteractor;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.IMainPresenter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public class MainPresenterImpl implements IMainPresenter, INewsInteractor.OnFinishedCallback {


    private HackerNewsView mView;
    private  INewsInteractor mNewsInteractor;

    public MainPresenterImpl(HackerNewsView view)
    {

        mView = view;
        setNewsInteractor(new NewsInteractor(this));

    }

    @Override
    public void setNewsInteractor(INewsInteractor newsInteractor) {
        mNewsInteractor = newsInteractor;
    }


    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }


    @Override
    public void fetchTopStoriesID() {
        mNewsInteractor.fetchTopStoriesID();
        mView.showProgress();
    }



    //callback implemented,inerator will call this method
    @Override
    public void onStoryRetrieved(TopStoryDTO topStoryDTO) {
        mView.addData(topStoryDTO);
        mView.hideProgress();
    }



    @Override
    public void onMessageIdsRetrieved(List<Integer> topStoriesIDs) {
        mView.setTopMessageIds(topStoriesIDs);
    }

    @Override
    public void fetchTopStory(Integer id) {
        if(id!=null)
        mNewsInteractor.fetchTopStory(id);
    }

    //callback implemented,inerator will call this method
    @Override
    public void onRetrievalFailed(String error) {
        mView.hideProgress();
        mView.showError(error);
    }
    @Override
    public void onCommentRetrieved(TopStoryDTO topStoryDTO) {
        throw new UnsupportedOperationException();
    }
}
