package com.example.gopaljee.hackernews2.presentations.presentors;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.base.BasePresenter;
import com.example.gopaljee.hackernews2.presentations.ui.HackerNewsBaseView;

import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public interface IMainPresenter extends BasePresenter {

    interface HackerNewsView extends HackerNewsBaseView
    {
      void addData(TopStoryDTO data );
        void setTopMessageIds(List<Integer> topStoriesIDs);
    }
    void fetchTopStoriesID();
    void fetchTopStory(Integer id);
    void setNewsInteractor(INewsInteractor newsInteractor);


}
