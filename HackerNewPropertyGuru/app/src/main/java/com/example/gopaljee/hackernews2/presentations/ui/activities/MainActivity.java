package com.example.gopaljee.hackernews2.presentations.ui.activities;

import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gopaljee.hackernews2.R;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.NewsStore;
import com.example.gopaljee.hackernews2.presentations.presentors.IMainPresenter;
import com.example.gopaljee.hackernews2.presentations.presentors.impl.MainPresenterImpl;
import com.example.gopaljee.hackernews2.presentations.ui.adapter.HackerNewsAdapter;

import java.util.List;

//try: Toast + tab
public class MainActivity extends AppCompatActivity implements IMainPresenter.HackerNewsView,SwipeRefreshLayout.OnRefreshListener{



    private MainPresenterImpl mMainPresenter;
    private RecyclerView mRecyclerView;
    HackerNewsAdapter mAdapter ;
    private ProgressBar mProgressBar;
    private SwipeRefreshLayout mSwipeRefreshLayout;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mMainPresenter = new MainPresenterImpl(this);
        mProgressBar = (ProgressBar)findViewById(R.id.activity_main_progressBar);
        mSwipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.refresh);
        mMainPresenter.fetchTopStoriesID();


    }
    @Override
    public void onRefresh() {
        mSwipeRefreshLayout.setRefreshing(true);

        mAdapter.clear();
        NewsStore.clear();
        mAdapter.notifyDataSetChanged();
        mMainPresenter.fetchTopStoriesID();
    }

    @Override
    public void setTopMessageIds(List<Integer> topStoriesIDs) {
        mAdapter = new HackerNewsAdapter(topStoriesIDs,mMainPresenter);
        mRecyclerView = (RecyclerView)findViewById(R.id.news_list_recycler);
        mRecyclerView.setAdapter(mAdapter);

        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
        mRecyclerView.setLayoutManager(layoutManager);
    }

    @Override
    public void addData(TopStoryDTO data) {
        if(data !=null) {
            mAdapter.add(data);
            NewsStore.Add(data);
            mAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
        mSwipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void showError(String message) {
        mProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }
}


