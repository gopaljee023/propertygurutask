package com.example.gopaljee.hackernews2.presentations;

import com.example.gopaljee.hackernews2.model.TopStoryDTO;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public class NewsStore {

    private static List<TopStoryDTO> _newsArticles;
    static {
        _newsArticles = new ArrayList<>();
    }

    public static void clear()
    {
        _newsArticles.clear();
    }
    public static List<TopStoryDTO> get_newsArticles() {
        return _newsArticles;
    }

    public static void Add(TopStoryDTO topStoryDTO)
    {
        _newsArticles.add(topStoryDTO);
    }
}