package com.example.gopaljee.hackernews2.presentations.presentors.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.domain.interactor.impl.NewsInteractor;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.ICommentPresentor;

import java.util.List;


/**
 * Created by gopaljee on 10/07/17.
 */

public class CommentPresenter implements ICommentPresentor ,INewsInteractor.OnFinishedCallback {


    private HackerCommentView mView;
    private INewsInteractor mNewsInteractor;
    public CommentPresenter(HackerCommentView view) {
        mView = view;
        setNewsInteractor(new NewsInteractor(this));
    }

    @Override
    public void setNewsInteractor(INewsInteractor newsInteractor) {
        mNewsInteractor = newsInteractor;
    }

    @Override
    public void fetchComment(List<Integer> id,int start) {
        if(id !=null && id.size()>start && start>=0) {
            mNewsInteractor.fetchTopStory(id.get(start));
        }
    }

    @Override
    public void resume() {

    }

    @Override
    public void pause() {

    }

    @Override
    public void stop() {

    }

    @Override
    public void destroy() {

    }

    @Override
    public void onError(String message) {

    }

    /**
     *
     * @param topStoriesIDs
     */
    @Override
    public void onMessageIdsRetrieved(List<Integer> topStoriesIDs) {
      throw new UnsupportedOperationException();
    }

    @Override
    public void onRetrievalFailed(String error) {
       mView.showError(error);
        mView.hideProgress();
    }

    @Override
    public void onStoryRetrieved(TopStoryDTO topStoryDTO) {

       mView.hideProgress(); //anyhow we have to hide progress bar
        if(topStoryDTO!=null){
        boolean isDeleted = topStoryDTO.getDeleted() == null?false:topStoryDTO.getDeleted();
        if(topStoryDTO!=null && !isDeleted )
        {
            CommentDTO dto = new CommentDTO();
            dto.setBy(topStoryDTO.getBy());
            dto.setTime(topStoryDTO.getTime());
            dto.setParentId(topStoryDTO.getParent());
            dto.setText(topStoryDTO.getText());
            dto.setKids(topStoryDTO.getKids());
            dto.setId(topStoryDTO.getId());

            mView.addData(dto,false);
            if(dto.getKids() !=null && dto.getKids().size()>0){
                mNewsInteractor.fetchComment(dto.getKids().get(0));
            }
        }}

    }


    @Override
    public void onCommentRetrieved(TopStoryDTO topStoryDTO) {
        if(topStoryDTO!=null) {
            boolean isDeleted = topStoryDTO.getDeleted() == null ? false : topStoryDTO.getDeleted();
            if (!isDeleted) {
                CommentDTO dto = new CommentDTO();
                dto.setBy(topStoryDTO.getBy());
                dto.setId(topStoryDTO.getId());
                dto.setTime(topStoryDTO.getTime());
                dto.setParentId(topStoryDTO.getParent());
                dto.setText(topStoryDTO.getText());
                mView.addData(dto, true);
            }
        }

    }
}
