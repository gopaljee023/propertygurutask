package com.example.gopaljee.hackernews2.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public class TopStoryDTO  {

    @SerializedName("by")
    @Expose
    private String by;

    @SerializedName("descendants")
    @Expose
    private Integer descendants;

    @SerializedName("id")
    @Expose
    private Integer id;

    @SerializedName("parent")
    @Expose
    private Integer parent;



    @SerializedName("score")
    @Expose
    private Integer score;  //The story's score, or the votes for a pollopt.




    @SerializedName("parts")
    @Expose
    private List<Integer> parts = null;// A list of related pollopts, in display order.Example: [126810, 126811, 126812]


    @SerializedName("kids")
    @Expose
    private List<Integer> kids = null;


    @SerializedName("time")
    @Expose
    private Integer time; //Example: 1172394646

    @SerializedName("title")
    @Expose
    private String title;

    @SerializedName("text")
    @Expose
    private String text;


    @SerializedName("type")
    @Expose
    private String type; //story,comment

    @SerializedName("url")
    @Expose
    private String url;

    public Boolean getDeleted() {
        return deleted;
    }

    public void setDeleted(Boolean deleted) {
        this.deleted = deleted;
    }

    @SerializedName("deleted")
    @Expose

    private Boolean deleted;

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }


    public Integer getDescendants() {
        return descendants;
    }

    public void setDescendants(Integer descendants) {
        this.descendants = descendants;
    }

    public Integer getId() {
        return id;
    }

    public Integer getParent() {
        return parent;
    }

    public void setParent(Integer parent) {
        this.parent = parent;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    public Integer getScore() {
        return score;
    }

    public List<Integer> getParts() {
        return parts;
    }

    public void setParts(List<Integer> parts) {
        this.parts = parts;
    }

    public void setScore(Integer score) {
        this.score = score;
    }

    public List<Integer> getKids() {
        return kids;
    }

    public void setKids(List<Integer> kids) {
        this.kids = kids;
    }


    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
