package com.example.gopaljee.hackernews2.model;

import java.util.List;

/**
 * Created by gopaljee on 10/07/17.
 */

public class CommentDTO {
    private String by;
    private List<Integer> kids = null;
    private String text;
    private Integer time;
    private List<CommentDTO> childs;
    private Integer parentId;
    private Integer Id;

    public Integer getId() {
        return Id;
    }

    public void setId(Integer id) {
        Id = id;
    }

    public Integer getParentId() {
        return parentId;
    }

    public void setParentId(Integer parentId) {
        this.parentId = parentId;
    }

    public String getBy() {
        return by;
    }

    public void setBy(String by) {
        this.by = by;
    }

    public List<Integer> getKids() {
        return kids;
    }

    public void setKids(List<Integer> kids) {
        this.kids = kids;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getTime() {
        return time;
    }

    public void setTime(Integer time) {
        this.time = time;
    }

    public List<CommentDTO> getChilds() {
        return childs;
    }

    public void setChilds(List<CommentDTO> childs) {
        this.childs = childs;
    }
}
