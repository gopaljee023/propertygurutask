package com.example.gopaljee.hackernews2.domain.interactor;

import com.example.gopaljee.hackernews2.model.TopStoryDTO;

import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public interface INewsInteractor {
    interface  OnFinishedCallback
    {
        void onMessageIdsRetrieved(List<Integer> topStoriesIDs);

        void onRetrievalFailed(String error);

        void onStoryRetrieved(TopStoryDTO topStoryDTO);
        void onCommentRetrieved(TopStoryDTO topStoryDTO);
    }
    void fetchTopStoriesID();
    void fetchTopStory(Integer newsId);
    void fetchComment(Integer newsId);
}
