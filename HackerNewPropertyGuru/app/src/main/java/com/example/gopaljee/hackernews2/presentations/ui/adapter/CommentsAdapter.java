package com.example.gopaljee.hackernews2.presentations.ui.adapter;

import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.gopaljee.hackernews2.R;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.ICommentPresentor;
import com.example.gopaljee.hackernews2.utils.DateUtils;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by gopaljee on 10/07/17.
 */

public class CommentsAdapter extends  RecyclerView.Adapter<CommentsAdapter.CommentHolder> {

    private List<CommentDTO> _dataSet;
    private ICommentPresentor _presentor;
    private List<Integer> _totalComments;
    public CommentsAdapter(List<Integer> kids, ICommentPresentor presentor)
    {


        _presentor = presentor;
        initializeDataSet();
        _totalComments = kids;
    }

    public void initializeDataSet()
    {
        _dataSet = new ArrayList<>();
    }
    public void add(CommentDTO story,boolean isChild)
    {
        if(isChild == false)
        {
            _dataSet.add(story);
        }
        else
        {
            for(CommentDTO d : _dataSet){
                if(d.getId() != null && d.getId().intValue() == story.getParentId().intValue())
                {
                    d.setChilds(Arrays.asList(story));
                }
            }
        }


    }
    public void updateComments(List<CommentDTO> comments){
        _dataSet.addAll(comments);
        notifyDataSetChanged();
    }

    @Override
    public CommentHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.comment_item,parent,false);
        return new CommentsAdapter.CommentHolder(view);
    }

    @Override
    public void onBindViewHolder(CommentHolder holder, int position) {
       if(position<_dataSet.size()) {
           CommentDTO article = _dataSet.get(position);

           holder.get_textReply().setText("reply");
           holder.get_textTime().setText(DateUtils.formatNewsApiDate(String.valueOf(article.getTime())));
           holder.get_textByUser().setText(article.getBy());

           if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
               holder.get_textDesc().setText(Html.fromHtml(Html.fromHtml(article.getText(),Html.FROM_HTML_MODE_LEGACY).toString(),Html.FROM_HTML_MODE_LEGACY));
           }
           else
           {
               holder.get_textDesc().setText(Html.fromHtml(Html.fromHtml(article.getText()).toString()));
           }

           int childCount = article.getChilds() == null ? 0 : article.getChilds().size();


           if (childCount == 0) {
               holder.get_linearLayout().setVisibility(View.INVISIBLE);
           } else {
               holder.get_linearLayout().setVisibility(View.VISIBLE);
               CommentDTO child = article.getChilds().get(0);
               holder.get_textTimeChild1().setText(DateUtils.formatNewsApiDate(String.valueOf(child.getTime())));
               holder.get_textByUserChild1().setText(child.getBy());
               holder.get_textDescChild1().setText(child.getText());
               holder.get_textReplyChild1().setText("reply");
           }
       }
       else
       {
           //_presentor ::fetch here
           _presentor.fetchComment(_totalComments,position);
       }

    }

    @Override
    public int getItemCount() {
        return  _totalComments == null?0:_totalComments.size();
    }

    public static class CommentHolder extends RecyclerView.ViewHolder {
        private TextView _textTime;
        private TextView _textDesc;
        private LinearLayout _linearLayout;
        private TextView _textTimeChild1;
        private TextView _textDescChild1;
        private TextView _textByUser;
        private TextView _textByUserChild1;
        private TextView _textReply;
        private TextView _textReplyChild1;

        public TextView get_textReply() {
            return _textReply;
        }

        public TextView get_textReplyChild1() {
            return _textReplyChild1;
        }

        public LinearLayout get_linearLayout() {
            return _linearLayout;
        }

        public TextView get_textTimeChild1() {
            return _textTimeChild1;
        }

        public TextView get_textDescChild1() {
            return _textDescChild1;
        }

        public TextView get_textDesc() {
            return _textDesc;
        }

        public TextView get_textTime() {
            return _textTime;
        }
        public TextView get_textByUser() {
            return _textByUser;
        }


        public TextView get_textByUserChild1() {
            return _textByUserChild1;
        }

        public CommentHolder(View itemView) {

            super(itemView);
            _textTime = (TextView)itemView.findViewById(R.id.comment_item_time);
            _textDesc = (TextView)itemView.findViewById(R.id.comment_item_text);
            _linearLayout = (LinearLayout)itemView.findViewById(R.id.comment_item_child_linearlayout);
            _textTimeChild1 = (TextView)itemView.findViewById(R.id.comment_item_time_child1);

            _textDescChild1 = (TextView)itemView.findViewById(R.id.comment_item_text_child1);
            _textByUser = (TextView)itemView.findViewById(R.id.comment_item_by_user);
            _textByUserChild1 =(TextView)itemView.findViewById(R.id.comment_item_by_userChild1);
            _textReply = (TextView)itemView.findViewById(R.id.comment_item_reply);
            _textReplyChild1 = (TextView)itemView.findViewById(R.id.comment_item_replyChild1);
        }

    }
}
