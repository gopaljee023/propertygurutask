package com.example.gopaljee.hackernews2.utils;

/**
 * Created by gopaljee on 03/07/17.
 */

public class CommentUtils {

    public static String countTotalCommentMessage(int totalNoOfComments)
    {
        String comment ="discuss";
        if(totalNoOfComments>0)
        {
            comment = String.valueOf(totalNoOfComments);
            String addendum = (totalNoOfComments==1)? " comment":" comments";
            comment = comment+ addendum ;
        }
        return comment;
    }

    public static String countTotalPointsMessage(int totalNoOfScore)
    {
        String comment ="";
        if(totalNoOfScore>0)
        {
            comment = String.valueOf(totalNoOfScore);

            String addendum = (totalNoOfScore==1)? " point":" points";
            comment = comment+ addendum ;
        }
        return comment;
    }
}