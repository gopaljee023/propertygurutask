package com.example.gopaljee.hackernews2.presentations.ui.activities;

import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.gopaljee.hackernews2.R;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.NewsStore;
import com.example.gopaljee.hackernews2.presentations.presentors.ICommentPresentor;
import com.example.gopaljee.hackernews2.presentations.presentors.impl.CommentPresenter;
import com.example.gopaljee.hackernews2.presentations.ui.HackerNewsBaseView;
import com.example.gopaljee.hackernews2.presentations.ui.adapter.CommentsAdapter;
import com.example.gopaljee.hackernews2.presentations.ui.adapter.HackerNewsAdapter;

public class CommentActivity extends AppCompatActivity implements ICommentPresentor.HackerCommentView {
    private static String KEY_INDEX = "keyindex";
    ICommentPresentor mCommentPresenter;
    private ProgressBar mProgressBar;

    private RecyclerView mRecyclerView;
    CommentsAdapter mAdapter ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comment);

        mProgressBar = (ProgressBar)findViewById(R.id.comments_progressBar) ;

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Comments");

        int index = getIntent().getIntExtra(KEY_INDEX,-1);
        if(index!=-1) {
            mCommentPresenter = new CommentPresenter(this);
            TopStoryDTO article = NewsStore.get_newsArticles().get(index);
            int count = article.getKids() == null?0:article.getKids().size();
         //   mCommentPresenter.fetchComment(article.getKids(),0);
            mAdapter = new CommentsAdapter(article.getKids(),mCommentPresenter);
            mRecyclerView = (RecyclerView)findViewById(R.id.comments_recycler);
            mRecyclerView.setAdapter(mAdapter);

            RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(this);
            mRecyclerView.setLayoutManager(layoutManager);
        }
        else
        {
            Toast.makeText(this,"Sorry incorrect index passed", Toast.LENGTH_SHORT).show();
        }

    }

    /**
     * implementation of HackerCommentView
     */
    @Override
    public void addData(CommentDTO data,boolean isChild) {
        if(data!=null) {
            mAdapter.add(data,isChild);
            mAdapter.notifyDataSetChanged();
        }
    }

    /**
     * implementation of HackerCommentView.HackerNewsBaseView
     */
    @Override
    public void showProgress() {
        mProgressBar.setVisibility(View.VISIBLE);
    }

    /**
     * implementation of HackerCommentView.HackerNewsBaseView
     */
    @Override
    public void hideProgress() {
        mProgressBar.setVisibility(View.INVISIBLE);
    }

    /**
     * implementation of HackerCommentView.HackerNewsBaseView
     */

    @Override
    public void showError(String message) {
        mProgressBar.setVisibility(View.INVISIBLE);
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public static void launch(Context context, int index)
    {
        Intent intent = new Intent(context,CommentActivity.class);
        intent.putExtra(KEY_INDEX,index);
        context.startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId())
        {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
