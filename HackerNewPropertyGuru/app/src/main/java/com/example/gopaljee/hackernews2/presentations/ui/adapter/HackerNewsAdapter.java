package com.example.gopaljee.hackernews2.presentations.ui.adapter;

import android.support.v7.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.gopaljee.hackernews2.R;
import com.example.gopaljee.hackernews2.model.*;
import com.example.gopaljee.hackernews2.presentations.presentors.IMainPresenter;
import com.example.gopaljee.hackernews2.presentations.ui.activities.CommentActivity;
import com.example.gopaljee.hackernews2.utils.CommentUtils;
import com.example.gopaljee.hackernews2.utils.DateUtils;
import com.example.gopaljee.hackernews2.utils.UserMessageUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by gopaljee on 03/07/17.
 */

public class HackerNewsAdapter extends  RecyclerView.Adapter<HackerNewsAdapter.NewsHolder>
{
    private List<TopStoryDTO> _dataSet;
    private List<Integer> topIds;
    private IMainPresenter mainPresenter;

    public void add(TopStoryDTO story)
    {
        if(story!=null) {
            _dataSet.add(story);
        }
    }
    public HackerNewsAdapter(List<Integer> topIds, IMainPresenter mainPresenter)
    {
        this.topIds = topIds;
        this.mainPresenter = mainPresenter;
        _dataSet = new ArrayList<>();
    }

    @Override
    public NewsHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.news_item,parent,false);
        return new NewsHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsHolder holder, final int position) {
        if(position<_dataSet.size()) {

            holder.get_textComments().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    CommentActivity.launch(v.getContext(), position);
                }
            });
            TopStoryDTO article = _dataSet.get(position);

            holder.get_textTitle().setText(article.getTitle());
            int score = article.getScore() == null ? 0 : article.getScore();
            holder.get_textPoints().setText(CommentUtils.countTotalPointsMessage(score));
            holder.get_textByUser().setText(UserMessageUtils.userMessage(article.getBy()));
            int commentCount = article.getKids() == null ? 0 : article.getKids().size();
            holder.get_textComments().setText(CommentUtils.countTotalCommentMessage(commentCount));

            holder.get_textCounter().setText(String.valueOf(position + 1) + ".");
            holder.get_textTime().setText(DateUtils.formatNewsApiDate(String.valueOf(article.getTime())));
        }
        else
        {
            mainPresenter.fetchTopStory(topIds.get(position));
        }

    }


    @Override
    public int getItemCount() {
        return topIds.size();
    }

    public void clear() {
        _dataSet.clear();
    }


    public static class NewsHolder extends RecyclerView.ViewHolder {

        private TextView _textTitle;
        private TextView _textCounter;
        private TextView _textTime;
        private TextView _textComments;
        private TextView _textByUser;
        private TextView _textPoints;

        public TextView get_textTitle() {
            return _textTitle;
        }

        public TextView get_textCounter() {
            return _textCounter;
        }

        public TextView get_textTime() {
            return _textTime;
        }

        public TextView get_textComments() {
            return _textComments;
        }

        public TextView get_textByUser() {
            return _textByUser;
        }

        public TextView get_textPoints() {
            return _textPoints;
        }

        public NewsHolder(View itemView) {
            super(itemView);
             _textCounter = (TextView)itemView.findViewById(R.id.card_news_position);
            _textTitle = (TextView)itemView.findViewById(R.id.card_news_title);
            _textTime = (TextView)itemView.findViewById(R.id.card_news_item_time);
            _textComments = (TextView)itemView.findViewById(R.id.card_news_item_kids_count);
            _textByUser = (TextView)itemView.findViewById(R.id.card_news_item_by_user);

            _textPoints =  (TextView)itemView.findViewById(R.id.card_news_item_score);

        }


    }
}
