package com.example.gopaljee.hackernews2.domain.interactor.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.domain.network.impl.NewsAPI;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;


import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by gopaljee on 03/07/17.
 */

public class NewsInteractor implements INewsInteractor {


    private INewsInteractor.OnFinishedCallback  mOnFinishedCallback;
    public NewsInteractor(INewsInteractor.OnFinishedCallback callback) {
        this.mOnFinishedCallback = callback;
    }
    Call<TopStoryDTO> getTopStoriesResponseCall;
    public void setTopStoriesResponseCall(Call<TopStoryDTO> getTopStoriesResponseCall)
    {
        this.getTopStoriesResponseCall = getTopStoriesResponseCall;
    }
    @Override
    public void fetchTopStory(Integer newsId) {
        NewsAPI.TopStoriesService  service = NewsAPI.getTopStoriesApi();
        Call<TopStoryDTO> getTopStoriesResponseCall = service.getStory(newsId);
        setTopStoriesResponseCall(getTopStoriesResponseCall);

        getTopStoriesResponseCall.enqueue(new Callback<TopStoryDTO>() {
            @Override
            public void onResponse(Call<TopStoryDTO> call, Response<TopStoryDTO> response) {
                TopStoryDTO topStoryDto = response.body();
                mOnFinishedCallback.onStoryRetrieved(topStoryDto);
            }

            @Override
            public void onFailure(Call<TopStoryDTO> call, Throwable t) {
                mOnFinishedCallback.onRetrievalFailed(t.getMessage());
            }
        });
    }

    public void fetchTopStoriesID()
    {
        Call<List<Integer>> getTopStoriesResponseCall = NewsAPI.getTopStoriesApi().getTopStoriesIds();
        getTopStoriesResponseCall.enqueue(new Callback<List<Integer>>() {
            @Override
            public void onResponse(Call<List<Integer>> call, Response<List<Integer>> response) {
                List<Integer> topStoriesIDs = response.body();
                mOnFinishedCallback.onMessageIdsRetrieved(topStoriesIDs);
            }

            @Override
            public void onFailure(Call<List<Integer>> call, Throwable t) {
                mOnFinishedCallback.onRetrievalFailed(t.getMessage());
            }
        });
    }

    @Override
    public void fetchComment(Integer newsId) {
        Call<TopStoryDTO> getTopStoriesResponseCall = NewsAPI.getTopStoriesApi().getStory(newsId);
        getTopStoriesResponseCall.enqueue(new Callback<TopStoryDTO>() {
            @Override
            public void onResponse(Call<TopStoryDTO> call, Response<TopStoryDTO> response) {
                TopStoryDTO topStoryDto = response.body();
                mOnFinishedCallback.onCommentRetrieved(topStoryDto);
            }

            @Override
            public void onFailure(Call<TopStoryDTO> call, Throwable t) {
                mOnFinishedCallback.onRetrievalFailed(t.getMessage());
            }
        });
    }
}
