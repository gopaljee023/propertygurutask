package com.example.gopaljee.hackernews2.presentations;

import com.example.gopaljee.hackernews2.model.TopStoryDTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 05/07/17.
 */


public class NewsStoreTest {

    @After
    public void tearDown()
    {
        NewsStore.clear();
    }
    @Test
    public void get_newsArticles_withoutSet_ZeroElement() throws Exception {

       assertEquals(0,NewsStore.get_newsArticles().size());

    }

    @Test
    public void get_newsArticles_AfterAdd_ReturnAllElements() throws Exception {


        NewsStore.Add(new TopStoryDTO());

        assertEquals(1,NewsStore.get_newsArticles().size());

    }


    @Test
    public void add_getShouldReturnSameItem() throws Exception {
        TopStoryDTO dto1 = new TopStoryDTO();

        NewsStore.Add(dto1);
        TopStoryDTO dto2 = new TopStoryDTO();
        NewsStore.Add(dto2);

        assertArrayEquals(Arrays.asList(dto1,dto2).toArray(),NewsStore.get_newsArticles().toArray());
    }

    @Test
    public void clear_ItemCountShouleBeZero()
    {
        NewsStore.clear();
        assertEquals(0,NewsStore.get_newsArticles().size());

    }
}