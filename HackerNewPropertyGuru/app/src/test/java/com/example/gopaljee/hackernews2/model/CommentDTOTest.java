package com.example.gopaljee.hackernews2.model;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 10/07/17.
 */
public class CommentDTOTest {

    private CommentDTO storyDTO;
    @Before
    public void setUp() throws Exception {
        storyDTO = new CommentDTO();
    }

    @After
    public void tearDown() throws Exception {
        storyDTO =null;
    }
    @Test
    public void getId_WithoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getId());
    }
    @Test
    public void getId_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setId(10);
        assertEquals(storyDTO.getId(),Integer.valueOf(10));
    }

    @Test
    public void setId_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setId(10);
        assertEquals(storyDTO.getId(),Integer.valueOf(10));
    }


    @Test
    public void getParentId() throws Exception {

    }

    @Test
    public void setParentId() throws Exception {

    }

    @Test
    public void getBy_beforeset_returnNull() throws Exception {
        //given

        //when
        String by = storyDTO.getBy();
        //then
        assertNull(by);
    }

    @Test
    public void getByAfterSet_returnsValid() throws Exception {
        //given
        String input ="John";
        storyDTO.setBy(input);
        //when
        String by = storyDTO.getBy();
        //then
        assertEquals(input,by);
    }

    @Test
    public void setBy_AfterSet_getReturnsSame() throws Exception {
        //given
        String input ="John";
        storyDTO.setBy(input);
        //when
        String by = storyDTO.getBy();
        //then
        assertEquals(input,by);
    }

    @Test
    public void getKids_withoutSet_returnDefault() throws Exception {
        assertNull(storyDTO.getKids());
    }

    @Test
    public void getKids_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setKids(integers);

        assertArrayEquals(storyDTO.getKids().toArray(),integers.toArray());

    }

    @Test
    public void setKids_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setKids(integers);

        assertArrayEquals(storyDTO.getKids().toArray(),integers.toArray());
    }

    @Test
    public void getText() throws Exception {

    }

    @Test
    public void setText() throws Exception {

    }

    @Test
    public void getTime_withoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getTime());
    }

    @Test
    public void getTime_witSet_ReturnsSame() throws Exception {
        storyDTO.setTime(1000);
        assertEquals(storyDTO.getTime(),Integer.valueOf(1000));
    }

    @Test
    public void setTime_afterSet_returnSame() throws Exception {
        storyDTO.setTime(1000);
        assertEquals(storyDTO.getTime(),Integer.valueOf(1000));
    }


    @Test
    public void getChilds_withoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getChilds());
    }

    @Test
    public void getChilds_withset_returnSame() throws Exception {
        CommentDTO dto = new CommentDTO();
        storyDTO.setChilds(Arrays.asList(dto));
        assertEquals(storyDTO.getChilds(),Arrays.asList(dto));
    }

    @Test
    public void setChilds_withset_returnsame() throws Exception {
        CommentDTO dto = new CommentDTO();
        storyDTO.setChilds(Arrays.asList(dto));
        assertEquals(storyDTO.getChilds(),Arrays.asList(dto));
    }

}