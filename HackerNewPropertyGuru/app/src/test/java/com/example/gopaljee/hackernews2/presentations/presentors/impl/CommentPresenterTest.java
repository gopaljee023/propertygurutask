package com.example.gopaljee.hackernews2.presentations.presentors.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.ICommentPresentor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 10/07/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class CommentPresenterTest {

    @Mock
    ICommentPresentor.HackerCommentView view;

    @Mock
    INewsInteractor mNewsInteractor;


    private CommentPresenter mCommentPresenter;
    TopStoryDTO dto;
    @Before
    public void setUp() throws Exception {
        // MockitoAnnotations.initMocks(this);
        mCommentPresenter = new CommentPresenter(view);
        dto= new TopStoryDTO();
        dto.setBy("gopal");
        dto.setTime(Integer.valueOf(100));
        dto.setParent(Integer.valueOf(200));
        dto.setText("hello");
        dto.setId(Integer.valueOf(300));
        dto.setKids(Arrays.asList(Integer.valueOf(400),Integer.valueOf(500)));
    }

    @After
    public void tearDown() throws Exception {
        mCommentPresenter = null;
    }


    @Test
    public void fetchComment_IdIsNull_ShouldNotExecute() throws Exception {
        List<Integer> ids =null;
        int start = 0;
        mCommentPresenter.fetchComment(ids,start);
        Mockito.verify(mNewsInteractor,Mockito.never()).fetchTopStory(Mockito.anyInt());
    }


    @Test
    public void fetchComment_idisvalid_but_startOutOfRange() throws Exception {
        List<Integer> ids = Arrays.asList(1,2,3,4,5);
        int start = 5;
        mCommentPresenter.fetchComment(ids,start);
        Mockito.verify(mNewsInteractor,Mockito.never()).fetchTopStory(Mockito.anyInt());
    }

    @Test
    public void fetchComment_idisvalid_but_startNegative() throws Exception {
        mCommentPresenter.setNewsInteractor(mNewsInteractor);

        List<Integer> ids = Arrays.asList(1,2,3,4,5);
        int start = -1;
        mCommentPresenter.fetchComment(ids,start);
        Mockito.verify(mNewsInteractor,Mockito.never()).fetchTopStory(Mockito.anyInt());
    }
    @Test
    public void fetchComment_idAndStartValid_SetStartAsZero() throws Exception {
      mCommentPresenter.setNewsInteractor(mNewsInteractor);

        List<Integer> ids = Arrays.asList(1,2,3,4,5);
        int start = 0;
        mCommentPresenter.fetchComment(ids,start);
        Mockito.verify(mNewsInteractor).fetchTopStory(1);

    }
    @Test
    public void fetchComment_idAndStartValid_SetStartAsLast() throws Exception {
        mCommentPresenter.setNewsInteractor(mNewsInteractor);

        List<Integer> ids = Arrays.asList(1,2,3,4,5);
        int start = 4;
        mCommentPresenter.fetchComment(ids,start);
        Mockito.verify(mNewsInteractor).fetchTopStory(5);

    }




    @Test(expected = RuntimeException.class)
    public void onMessageIdsRetrieved() throws Exception {
      mCommentPresenter.onMessageIdsRetrieved(null);
    }

    @Test
    public void onRetrievalFailed() throws Exception {

        String error = "error";
        mCommentPresenter.onRetrievalFailed(error);
        Mockito.verify(view).showError(error);
        Mockito.verify(view).hideProgress();
    }

    @Test
    public void onStoryRetrieved_topStoryNull() throws Exception {

        mCommentPresenter.onStoryRetrieved(null);
        Mockito.verify(view).hideProgress();

    }

    @Test
    public void onStoryRetrieved_validTopStoryWithKidsNull() throws Exception {
      dto.setKids(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();


    }
    @Test
    public void onStoryRetrieved_validTopStoryWithKidsZero() throws Exception {
        List<Integer> list = new ArrayList<>();
        dto.setKids(list);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();
    }
    @Test
    public void onStoryRetrieved_validTopStoryWithUserNull() throws Exception {
        dto.setBy(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();

    }
    @Test
    public void onStoryRetrieved_validTopStoryWithGetTimeNull() throws Exception {
        dto.setTime(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();
    }
    @Test
    public void onStoryRetrieved_validTopStoryWithGetParentNull() throws Exception {

        dto.setParent(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();
    }

    @Test
    public void onStoryRetrieved_validTopStoryWithGetTextNull() throws Exception {
        dto.setText(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();
    }

    @Test
    public void onStoryRetrieved_validTopStoryWithGetIdnull() throws Exception {
        dto.setId(null);
        mCommentPresenter.onStoryRetrieved(dto);
        Mockito.verify(view).hideProgress();
    }
    @Test
    public void onStoryRetrieved_validTopStoryWithAllValueSet() throws Exception {
        mCommentPresenter.setNewsInteractor(mNewsInteractor);
        mCommentPresenter.onStoryRetrieved(dto);

     //   Mockito.verify(view).addData(Mockito.any(CommentDTO.class),false);
       Mockito.verify(mNewsInteractor).fetchComment(400);
    }

    @Test
    public void onCommentRetrieved_ArgumentAsNull() throws Exception {
         mCommentPresenter.onCommentRetrieved(null);

    }
    @Test
    public void onCommentRetrieved_ValidArgument_WithGetByNull() throws Exception {
       dto.setBy(null);
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());

    }
    @Test
    public void onCommentRetrieved_ValidArgument_WithGetIdNull() throws Exception {
        dto.setId(null);
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());

    }
    @Test
    public void onCommentRetrieved_ValidArgument_WithGetTimeNull() throws Exception {
        dto.setTime(null);
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());
    }
    @Test
    public void onCommentRetrieved_ValidArgument_WithGetParentNull() throws Exception {
        dto.setParent(null);
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());
    }

    @Test
    public void onCommentRetrieved_ValidArgument_WithGetTextNull() throws Exception {
        dto.setText(null);
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());
    }


    @Test
    public void onCommentRetrieved_ValidArgument() throws Exception {
        mCommentPresenter.onCommentRetrieved(dto);

        Mockito.verify(view).addData(Mockito.any(CommentDTO.class),Mockito.anyBoolean());
    }

}