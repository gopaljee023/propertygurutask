package com.example.gopaljee.hackernews2.presentations.presentors.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.IMainPresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 04/07/17.
 */
@RunWith(MockitoJUnitRunner.class)
public class MainPresenterImplTest {


    @Mock
    IMainPresenter.HackerNewsView view;

    @Mock
    INewsInteractor mNewsInteractor;

    @InjectMocks
    private MainPresenterImpl mMainPresenter;


    @Before
    public void setUp() throws Exception {
      // MockitoAnnotations.initMocks(this);
       mMainPresenter = new MainPresenterImpl(view);
    }

    @Test
    public void fetchTopStoriesID() throws Exception {

        //given
        mMainPresenter.setNewsInteractor(mNewsInteractor);

        //when
        mMainPresenter.fetchTopStoriesID();

        //then
        Mockito.verify(mNewsInteractor).fetchTopStoriesID();
        Mockito.verify(view).showProgress();

    }


    @Test
    public void onStoryRetrieved_inputTopStoryDTO_PassedToView()
    {
        //given
        TopStoryDTO storyDTO = new TopStoryDTO();

        //when
        mMainPresenter.onStoryRetrieved(storyDTO);

        //then
        Mockito.verify(view).addData(storyDTO);
        Mockito.verify(view).hideProgress();
    }

    @Test
    public void onMessageIdsRetrieved_ValidInput()
    {

        List<Integer> topStoriesIDs = Arrays.asList(1,2,3);
        mMainPresenter.onMessageIdsRetrieved(topStoriesIDs);

        Mockito.verify(view).setTopMessageIds(Arrays.asList(1,2,3));

    }

    @Test
    public void onMessageIdsRetrieved_ZeroItems()
    {

        List<Integer> topStoriesIDs = new ArrayList<>();
        mMainPresenter.onMessageIdsRetrieved(topStoriesIDs);
        Mockito.verify(view).setTopMessageIds(topStoriesIDs);
    }

    @Test
    public void  onMessageIdsRetrieved_nullInput()
    {
        mMainPresenter.onMessageIdsRetrieved(null);
        Mockito.verify(view).setTopMessageIds(null);
    }

    @Test
    public void onRetrievalFailed()
    {
        mMainPresenter.onRetrievalFailed("error");

        Mockito.verify(view).hideProgress();
        Mockito.verify(view).showError(Mockito.anyString());
    }

    @Test
    public void fetchTopStory_invalidInput()
    {
        mMainPresenter.setNewsInteractor(mNewsInteractor);

        Mockito.verify(mNewsInteractor,Mockito.never()).fetchTopStory(Mockito.anyInt());
    }

    @Test
    public void fetchTopStory_validInput()
    {
        mMainPresenter.setNewsInteractor(mNewsInteractor);
        mMainPresenter.fetchTopStory(Integer.valueOf(1000));
        Mockito.verify(mNewsInteractor).fetchTopStory(Mockito.anyInt());
    }

    @Test(expected = RuntimeException.class)
    public void onCommentRetrieved()
    {
        mMainPresenter.onCommentRetrieved(null);
    }
}



