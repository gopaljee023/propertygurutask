package com.example.gopaljee.hackernews2.model;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 04/07/17.
 */
public class TopStoryDTOTest {

    private TopStoryDTO storyDTO;

    @Before
    public void setup() throws  Exception
    {
        storyDTO = new TopStoryDTO();
    }
    @Test
    public void getBy_beforeset_returnNull() throws Exception {
         //given

         //when
        String by = storyDTO.getBy();
        //then
        assertNull(by);
    }

    @Test
    public void getByAfterSet_returnsValid() throws Exception {
        //given
        String input ="John";
        storyDTO.setBy(input);
        //when
        String by = storyDTO.getBy();
        //then
        assertEquals(input,by);
    }

    @Test
    public void setBy_AfterSet_getReturnsSame() throws Exception {
        //given
        String input ="John";
        storyDTO.setBy(input);
        //when
        String by = storyDTO.getBy();
        //then
        assertEquals(input,by);
    }

    @Test
    public void getDescendants_WithoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getDescendants());
    }

    @Test
    public void getDescendants_WithSet_ReturnsSetValue() throws Exception {
       storyDTO.setDescendants(10);
        assertEquals(Integer.valueOf(10),storyDTO.getDescendants());
    }

    @Test
    public void setDescendants_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setDescendants(10);
        assertEquals(storyDTO.getDescendants(),Integer.valueOf(10));
    }

    @Test
    public void getId_WithoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getId());
    }
    @Test
    public void getId_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setId(10);
        assertEquals(storyDTO.getId(),Integer.valueOf(10));
    }

    @Test
    public void setId_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setId(10);
        assertEquals(storyDTO.getId(),Integer.valueOf(10));
    }


    @Test
    public void getScore_WithoutSet_ReturnsDefault() throws Exception {

        assertNull(storyDTO.getScore());

    }

    @Test
    public void getScore_AfterSet_getReturnsSame() throws Exception {
        storyDTO.setScore(10);
        assertEquals(storyDTO.getScore(),Integer.valueOf(10));
    }
    @Test
    public void setScore_returnsSameAsGet() throws Exception {
        storyDTO.setScore(10);
        assertEquals(storyDTO.getScore(),Integer.valueOf(10));
    }
    @Test
    public void getParts_withoutSet_returnDefault() throws Exception {
       assertNull(storyDTO.getParts());
    }

    @Test
    public void getParts_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setParts(integers);

        assertArrayEquals(storyDTO.getParts().toArray(),integers.toArray());

    }

    @Test
    public void setParts_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setParts(integers);

        assertArrayEquals(storyDTO.getParts().toArray(),integers.toArray());
    }



    @Test
    public void getKids_withoutSet_returnDefault() throws Exception {
        assertNull(storyDTO.getKids());
    }

    @Test
    public void getKids_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setKids(integers);

        assertArrayEquals(storyDTO.getKids().toArray(),integers.toArray());

    }

    @Test
    public void setKids_AfterSet_returnSame() throws Exception {
        List<Integer> integers = Arrays.asList(1,2,3);
        storyDTO.setKids(integers);

        assertArrayEquals(storyDTO.getKids().toArray(),integers.toArray());
    }

    @Test
    public void getTime_withoutSet_ReturnsDefault() throws Exception {
     assertNull(storyDTO.getTime());
    }

    @Test
    public void getTime_witSet_ReturnsSame() throws Exception {
        storyDTO.setTime(1000);
        assertEquals(storyDTO.getTime(),Integer.valueOf(1000));
    }

    @Test
    public void setTime_afterSet_returnSame() throws Exception {
        storyDTO.setTime(1000);
        assertEquals(storyDTO.getTime(),Integer.valueOf(1000));
    }

    @Test
    public void getTitle_withoutSet_ReturnsDefault() throws Exception {
      assertNull(storyDTO.getTitle());
    }
    @Test
    public void getTitle_withSet_ReturnsSetValue() throws Exception {
        storyDTO.setTitle("Hacker");
        assertEquals(storyDTO.getTitle(),"Hacker");
    }

    @Test
    public void setTitle_returnsSameAsSet() throws Exception {
        storyDTO.setTitle("Hacker");
        assertEquals(storyDTO.getTitle(),"Hacker");
    }

    @Test
    public void getType_withoutSet_ReturnsDefault() throws Exception {
        assertNull(storyDTO.getType());
    }
    @Test
    public void getType_withSet_ReturnsSetValue() throws Exception {
        storyDTO.setType("Comment");
        assertEquals(storyDTO.getType(),"Comment");
    }

    @Test
    public void setType_returnsSameAsSet() throws Exception {
        storyDTO.setType("Comment");
        assertEquals(storyDTO.getType(),"Comment");
    }


    @Test
    public void getUrl_withoutSet_ReturnDefault() throws Exception {
        assertNull(storyDTO.getUrl());
    }
    @Test
    public void getUrl_withSet_ReturnsSetValue() throws Exception {
        storyDTO.setUrl("www.google.com");
        assertEquals(storyDTO.getUrl(),"www.google.com");
    }

    @Test
    public void setUrl() throws Exception {
        storyDTO.setUrl("www.google.com");
        assertEquals(storyDTO.getUrl(),"www.google.com");
    }

}