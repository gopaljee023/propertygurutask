package com.example.gopaljee.hackernews2.domain.network;

import android.provider.CallLog;

import com.example.gopaljee.hackernews2.domain.network.impl.NewsAPI;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;



import java.util.Arrays;
import java.util.List;

import retrofit2.Call;
import retrofit2.http.Path;

/**
 * Created by gopaljee on 05/07/17.
 */
public class NewsAPITest {



    private class MockRetroFitServie implements NewsAPI.TopStoriesService
    {

        @Override
        public Call<List<Integer>> getTopStoriesIds() {
             return null;
        }

        @Override
        public Call<TopStoryDTO> getStory(@Path("id") int id) {
            return null;
        }
    }

}