package com.example.gopaljee.hackernews2.utils;

import org.junit.Test;

import static org.hamcrest.core.Is.is;
import static org.junit.Assert.*;

/**
 * Created by gopaljee on 04/07/17.
 */
public class UserMessageUtilsTest {

    @Test
    public void UserMessageUtils_inputEmptyUser_outPutEmptyString()
    {
        String expected = "";
        String actual = UserMessageUtils.userMessage("");
        assertEquals(expected,actual);
    }

    @Test
    public void UserMessageUtils_inputNullString_outputEmptryString()
    {
        String expected = "";
        String actual = UserMessageUtils.userMessage(null);
        assertEquals(expected,actual);
    }

    @Test
    public void UserMessageUtils_inputValidUserName_outputValidUserNameWithBy()
    {
        String expected = "by gopal";
        String actual = UserMessageUtils.userMessage("gopal");
        assertThat(expected, is(actual));
    }


    @Test
    public void UserMessageUtils_inputValidUserNameWithSpaceBothSide_outputValidUserNameWithBy()
    {
        String expected = "by gopal";
        String actual = UserMessageUtils.userMessage(" gopal ");
        assertThat(expected, is(actual));
    }

    @Test
    public void UserMessageUtils_inputValidUserNameWithSpaceInBetween_outputValidUserNameWithBy()
    {
        String expected = "by gopal jee";
        String actual = UserMessageUtils.userMessage(" gopal jee");
        assertThat(expected, is(actual));
    }

}