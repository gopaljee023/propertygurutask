package com.example.gopaljee.hackernews2.presentations.ui.adapter;

import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.presentations.presentors.ICommentPresentor;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * Created by gopaljee on 10/07/17.
 */
@RunWith(PowerMockRunner.class)
@PrepareForTest ( { CommentsAdapter.class })
public class CommentsAdapterTest {


    @Mock
    private ArrayList<Integer> kids;
    @Mock
    private ICommentPresentor _presentor;


    public CommentsAdapter mCommentsAdapter;

    @Before
    public void setUp() throws Exception {

        mCommentsAdapter = PowerMockito.mock(CommentsAdapter.class);
        PowerMockito.when(mCommentsAdapter.hasStableIds()).thenReturn(true);

    }

    @After
    public void tearDown() throws Exception {
        mCommentsAdapter = null;
    }

    @Test
    public void add_ifIsChildFalse_WithValidCommenDTO() throws Exception {
        CommentDTO dto = new CommentDTO();
        mCommentsAdapter.initializeDataSet();
        mCommentsAdapter.add(dto,false);
        //Mockito.verify(_dataSet).add(dto);
    }

    @Test
    public void updateComments() throws Exception {

    }

    @Test
    public void onCreateViewHolder() throws Exception {

    }

    @Test
    public void onBindViewHolder() throws Exception {

    }

    @Test
    public void getItemCount() throws Exception {

    }

}