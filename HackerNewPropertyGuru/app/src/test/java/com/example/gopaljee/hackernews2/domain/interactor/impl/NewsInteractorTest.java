package com.example.gopaljee.hackernews2.domain.interactor.impl;

import com.example.gopaljee.hackernews2.domain.interactor.INewsInteractor;
import com.example.gopaljee.hackernews2.domain.network.impl.NewsAPI;
import com.example.gopaljee.hackernews2.model.CommentDTO;
import com.example.gopaljee.hackernews2.model.TopStoryDTO;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.text.ParseException;

import retrofit2.Call;

import static org.junit.Assert.*;
import static org.mockito.Mockito.when;

/**
 * Created by gopaljee on 04/07/17.
 */

@RunWith(PowerMockRunner.class)
@PrepareForTest({NewsAPI.class})
public class NewsInteractorTest {
    @Mock
    private INewsInteractor.OnFinishedCallback  mOnFinishedCallback;
    @Mock
    private Call<TopStoryDTO> mUserResponseCall;

    @Mock
    NewsAPI.TopStoriesService  service;

    INewsInteractor  newsInteractor;
    @Before
    public void setUp() throws Exception {
        newsInteractor = new NewsInteractor(mOnFinishedCallback);
    }

    @After
    public void tearDown() throws Exception {
        newsInteractor =null;
    }
    @Test
    public void fetchTopStory() throws Exception {

    /*  when(service.getStory(1000)).thenReturn(mUserResponseCall);
        PowerMockito.mockStatic(NewsAPI.class);// this will make sure that FireBaseCrash.report() should n't crash
        newsInteractor.fetchTopStory(1000);


        PowerMockito.verifyStatic(); //This and next line will make sure that FireBaseCrash.report() called in the method.
        NewsAPI.getTopStoriesApi().getStory(1000);*/

    }

    @Test
    public void fetchTopStoriesID() throws Exception {

    }

}