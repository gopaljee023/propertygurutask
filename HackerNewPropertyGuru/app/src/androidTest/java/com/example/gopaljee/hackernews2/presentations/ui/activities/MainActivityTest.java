package com.example.gopaljee.hackernews2.presentations.ui.activities;


import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.View;

import com.example.gopaljee.hackernews2.R;


import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static junit.framework.Assert.assertNotNull;

/*Command+option+T,O(open),H(help),V(Variable introduce),

/**
 * Created by gopaljee on 05/07/17.
 */
//@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    /*Create a Rule annotated ActivityTestRule to get access to an Activity
    * must be public*/
    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);


    private MainActivity mActivity;
    /*setupAnd tearDown: must be public, your testcases must be public*/
    @Before
    public void setUp() throws Exception {
        mActivity = mActivityTestRule.getActivity();

    }

    @After
    public void tearDown() throws Exception {
        mActivity =null;
    }


    @Test
    public void shouldLaunchActivity()
    {
        //if we are able to get an item id from view..that means we are able to launch the activity.
        /*if we are able to get id of
         <android.support.v7.widget.RecyclerView
        android:layout_width="match_parent"
        android:layout_height="match_parent"
        android:id="@+id/news_list_recycler"
        />
        that means activity is launched.
         */

        View view = mActivity.findViewById(R.id.news_list_recycler);
        assertNotNull(view);
        mActivity.getActionBar();


    }
 //command+option+O,T,H (=help)
    @Test
    public void shouldTestTitle()
    {
        Context context = InstrumentationRegistry.getContext();


    }
    @Test
    public void onCreate() throws Exception {

    }

    @Test
    public void addData() throws Exception {

    }

    @Test
    public void showProgress() throws Exception {

    }

    @Test
    public void hideProgress() throws Exception {

    }

    @Test
    public void showError() throws Exception {

    }

}